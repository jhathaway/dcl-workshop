---
author: Jesse Hathaway
patat:
  margins:
    left: 4
    right: 4
  wrap: true
title: Data Center Labratory workshop
---

# Data Center Labratory workshop

## Workshop patches and deck

- <https://gitlab.wikimedia.org/jhathaway/dcl-workshop>

## Installation

- <https://gitlab.wikimedia.org/jhathaway/dcl#install>

## Brief Overview

- Provides a way to spin up local infrastructure develpoment
  environments, which mimic our production data centers
- Leverages Minikube to create a container for each host
- At start DCL provides you with a Puppetserver, PuppetDB & PKI server
- Your puppet repository is NFS mounted into the Puppetserver container,
  so there is no need to commit changes

## Puppet a host

    $ ssh pki1001
    $ sudo puppet agent -t

- Our Puppet agent systemd timer is disabled by default
- Using `entr` is a nice way to watch for edits and then run the agent

## Upgrade memcache to bookworm

    # Create a memcache host
    $ dcl add nodes -c bookworm mc1037.eqiad.wmnet
    $ ssh mc1037
    (mc1037) $ sudo puppet agent -t

- Newly provisioned nodes use a container with the insetup::container,
  so they are identical before the first Puppet run

## Missing memkeys package?

    (mc1037) $ sudo puppet agent -t

- We package `memkeys` ourselves, and we don't have a bookworm version,
  yet.
- Publish a new version to our apt repo

## Test memcache

    (mc1037) $ nc -C localhost 11211
    (mc1037) $ openssl s_client -quiet -connect mc1037:11214

# Are we done?

## Phabricator tickets!!

    1.  Change memcached daemon user from nobody to memcache
        - <https://phabricator.wikimedia.org/T273950>
    2.  Use our CFSSL PKI, rather than Puppet's PKI
        - <https://phabricator.wikimedia.org/T353511>

## Change daemon user

    $ git am 0001-memcached-change-daemon-user-to-memcache.patch
    $ git show
    (mc1037) $ sudo puppet agent -t
    (mc1037) $ sudo systemctl restart memcached
    (mc1037) $ openssl s_client -quiet -connect mc1037:11214

## Change PKI cert source

    $ git am 0002-memcached-switch-cert-to-CFSSL-PKI.patch
    $ git diff
    (mc1037) $ sudo puppet agent -t
    (mc1037) $ sudo systemctl restart memcached
    (mc1037) $ openssl s_client -quiet -connect mc1037:11214

# Questions?
