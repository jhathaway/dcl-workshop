# Notes

## memkeys package

    # build
    WIKIMEDIA=yes GBP_PBUILDER_DIST=bookworm DIST=bookworm GIT_PBUILDER_AUTOCONF=no gbp buildpackage -jauto -us -uc -sa --git-builder=git-pbuilder --git-debian-branch=debian --git-upstream-branch=master

    # import
    reprepro -C main include bookworm-wikimedia memkeys_20181031-3+deb12+wmf1_amd64.changes

    # remove
    reprepro remove bookworm-wikimedia memkeys
